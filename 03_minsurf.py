import matplotlib.pyplot as plt
from firedrake import *
import time

mesh = UnitSquareMesh(80, 80)
V = FunctionSpace(mesh, "CG", 1)

v = TestFunction(V)

x, y = SpatialCoordinate(mesh)
# Dirichlet BC on top and bottom
bc1 = DirichletBC(V, -0.5*sin(2*pi*y), 1) #left
bc2 = DirichletBC(V,  0.5*sin(2*pi*y), 2) #right
bc3 = DirichletBC(V, -0.0*sin(2*pi*x), 3) #bottom
bc4 = DirichletBC(V,  0.0*sin(2*pi*x), 4) #top

u = Function(V)

Psi = sqrt(1 + (inner(grad(u),grad(u))) )
Pi = Psi*dx

F = derivative(Pi,u,v)

problem = NonlinearVariationalProblem(F, u, bcs = [bc1 ,bc2, bc3, bc4])
solver = NonlinearVariationalSolver(problem, solver_parameters={"snes_atol": 1e-8})

t_start = time.time()
solver.solve()
elapsed = time.time() - t_start

print('Elapsed: {:.5e}'.format(elapsed))

File("minsurf.pvd").write(u)
