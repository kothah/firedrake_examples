import matplotlib.pyplot as plt
from firedrake import *
import time
from sys import argv

N=int(argv[1])
mesh = UnitSquareMesh(N, N)
V = FunctionSpace(mesh, "CG", 2)
Cval = 1
epsilon=1e-2

params_newton={"snes_type": "newtonls", 
        "ksp_type": "gmres",
        "pc_type": "hypre",
        "snes_atol": 1e-12,
        "snes_rtol": 1e-8,
        "snes_stol": 0.0,
        "snes_converged_reason": None,
        "snes_error_if_not_converged": False,
        #"ksp_monitor": None,
        "snes_max_it": 100}

params_qn={"snes_type": "qn", 
        "snes_qn_type": "lbfgs", 
        "snes_qn_m": 50, 
        "snes_qn_scale_type": "jacobian", 
        "snes_qn_restart_type": "none", 
        "ksp_type": "gmres",
        "ksp_norm_type": "none",
        "ksp_max_it": 5,
        "pc_type": "hypre",
        "snes_atol": 1e-12,
        "snes_rtol": 1e-8,
        "snes_stol": 0.0,
        "snes_converged_reason": None,
        "snes_error_if_not_converged": False,
        #"ksp_monitor": None,
        "snes_max_it": 200}

params_newton_npc={"snes_type": "anderson",
        "snes_converged_reason": None,
        "snes_monitor": None,
        "npc": {"snes_type": "newtonls", 
           "ksp_type": "gmres",
           "pc_type": "hypre",
           "snes_atol": 1e-12,
           "snes_rtol": 1e-8,
           "snes_stol": 0.0,
           #"snes_monitor": None,
           #"ksp_monitor": None,
           "snes_max_it": 1
           }
        }

params_qn_npc={"snes_type": "anderson",
        "snes_converged_reason": None,
        "snes_monitor": None,
        "npc": {"snes_type": "qn", 
            "snes_qn_type": "lbfgs", 
            "snes_qn_scale_type": "jacobian", 
            "ksp_type": "preonly",
            "pc_type": "hypre",
            "snes_atol": 0.0,
            "snes_rtol": 0.0,
            "snes_stol": 0.0,
            "snes_max_it": 5
            #"snes_converged_reason": None,
            #"ksp_monitor": None,
            }
        }

params_set = [params_newton, params_qn, params_newton_npc, params_qn_npc]
v = TestFunction(V)

x, y = SpatialCoordinate(mesh)
# Dirichlet BC on top and bottom
C=Constant(Cval)
bc1 = DirichletBC(V, -0.5*sin(2*C*pi*y), 1) #left
bc2 = DirichletBC(V,  0.5*sin(2*C*pi*y), 2) #right
bc3 = DirichletBC(V, -0.5*sin(2*C*pi*x), 3) #bottom
bc4 = DirichletBC(V,  0.5*sin(2*C*pi*x), 4) #top

u = Function(V)
du = TrialFunction(V)

Psi = sqrt(1 + (inner(grad(u),grad(u))) )
Pi = Psi*dx
F = derivative(Pi,u,v)
J = derivative(F, u, du)

Psi_stab = sqrt(1 + (inner(grad(u),grad(u))) ) + 0.5 * Constant(epsilon) * inner(grad(u), grad(u))
Pi_stab = Psi_stab*dx
F_stab = derivative(Pi_stab,u,v)
J_stab = derivative(F_stab, u, du)

problem = NonlinearVariationalProblem(F, u, J=J_stab, bcs = [bc1 ,bc2, bc3, bc4])
times = []
for params in params_set:
    u.interpolate(Constant(0.0))
    solver = NonlinearVariationalSolver(problem, solver_parameters=params)
    t_start = time.time()
    solver.solve()
    elapsed = time.time() - t_start
    times.append(elapsed)
    #print('Elapsed: {:.5e}'.format(elapsed))

names = ["Newton", "QN", "Newton-NPC", "QN-NPC"]
for t,n in zip(times, names):
    print(n, "{:.3e}".format(t))
File("minsurf.pvd").write(u)
