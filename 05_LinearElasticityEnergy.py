import matplotlib.pyplot as plt
from firedrake import *
import time

# Geometric information
length = 1
width = 0.2
mesh = RectangleMesh(40, 20, length, width)
# mesh = Mesh('square_tri4.e', dim=2)

# function space
V = VectorFunctionSpace(mesh, "CG", 1)

# boundary conditions
bc = DirichletBC(V, Constant([0, 0]), 1)
# bc = DirichletBC(V, 0.*cos(x*pi*2)*cos(y*pi*2), 1)

# constants
rho = Constant(0.01)
g = Constant(1)
f = as_vector([0, -rho*g])
mu = Constant(1)
lambda_ = Constant(0.25)
Id = Identity(mesh.geometric_dimension())

# define strain


def epsilon(u):
    return 0.5*(grad(u) + grad(u).T)


# Total potential energy

u = Function(V)
v = TestFunction(V)

psi = (lambda_/2)*(tr(epsilon(u))**2) + mu*inner(epsilon(u), epsilon(u))
Pi = psi*dx - dot(f, u)*dx
F = derivative(Pi, u, v)
# a = inner(sigma(u), epsilon(v)) * dx
# L = inner(f, v) * dx


problem = NonlinearVariationalProblem(F, u, bcs=bc)
solver = NonlinearVariationalSolver(problem, solver_parameters={
    "ksp_monitor": None, "snes_monitor": None, "pc_type": "lu",
    "snes_atol": 1e-8})

t = time.time()

# solve(a == L, u, solver_parameters={'ksp_type': 'cg', 'pc_type': 'none'})

# solve(a == L, u, solver_parameters={'ksp_type': 'cg', 'pc_type': 'ilu'})
# solve(a == L, uh, bcs=bc, solver_parameters={
#    'ksp_type': 'cg', 'pc_type': 'hypre', 'pc_hypre_type': 'boomeramg', 'ksp_monitor': None})
solver.solve()

elapsed = time.time() - t

print('Elapsed: {:.5e}'.format(elapsed))

File("elasticity.pvd").write(u)
