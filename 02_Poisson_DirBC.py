import matplotlib.pyplot as plt
from firedrake import *
import time

mesh = UnitSquareMesh(80, 80)
V = FunctionSpace(mesh, "CG", 1)

u = TrialFunction(V)
v = TestFunction(V)

f = Function(V)
x, y = SpatialCoordinate(mesh)
# f.interpolate(10*exp(-(pow(x - 0.5, 2) + pow(y - 0.5, 2)) / 0.02))
f.interpolate(8*pi*pi*cos(2*pi*x)*cos(2*pi*y))

# Dirichlet BC on top and bottom
bc1 = DirichletBC(V, cos(x*pi*2)*cos(y*pi*2), 1)
bc2 = DirichletBC(V, cos(x*pi*2)*cos(y*pi*2), 2)
bc3 = DirichletBC(V, cos(x*pi*2)*cos(y*pi*2), 3)
bc4 = DirichletBC(V, cos(x*pi*2)*cos(y*pi*2), 4)


a = (inner(grad(u), grad(v)) + inner(u, v)) * dx
L = inner(f, v) * dx

u_h = Function(V)
u_exact = Function(V)
u_h.rename("u_h")
u_exact.rename("u_exact")

# exact solution
u_exact.interpolate(cos(x*pi*2)*cos(y*pi*2))

t_start = time.time()
## solve the problem
# solve(a == L, u, solver_parameters={'ksp_type': 'cg', 'pc_type': 'none'})
# solve(a == L, u, solver_parameters={'ksp_type': 'cg', 'pc_type': 'ilu'})
solve(a == L, u_h, bcs=[bc1 ,bc2, bc3, bc4], solver_parameters={'ksp_type': 'cg', 'pc_type': 'hypre', 'pc_hypre_type': 'boomeramg'})

elapsed = time.time() - t_start

print('Elapsed: {:.5e}'.format(elapsed))

file = File("poisson.pvd")
file.write(u_h,u_exact)

#try:
#  import matplotlib.pyplot as plt
#except:
#  warning("Matplotlib not imported")
#
#try:
#  fig, axes = plt.subplots()
#  colors = tripcolor(u, axes=axes)
#  fig.colorbar(colors)
#except Exception as e:
#  warning("Cannot plot figure. Error msg: '%s'" % e)
#
#try:
#  fig, axes = plt.subplots()
#  contours = tricontour(u, axes=axes)
#  fig.colorbar(contours)
#except Exception as e:
#  warning("Cannot plot figure. Error msg: '%s'" % e)
#
#try:
#  plt.show()
#except Exception as e:
#  warning("Cannot show figure. Error msg: '%s'" % e)

print('Error L2 norm: {:.5e}'.format(sqrt(assemble(dot(u_h - u_exact, u_h - u_exact) * dx))))
