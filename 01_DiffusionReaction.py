import matplotlib.pyplot as plt
from firedrake import *
import time

mesh = UnitSquareMesh(800, 800)
V = FunctionSpace(mesh, "CG", 1)

u = TrialFunction(V)
v = TestFunction(V)

f = Function(V)
x, y = SpatialCoordinate(mesh)
f.interpolate((1+8*pi*pi)*cos(x*pi*2)*cos(y*pi*2))

a = (inner(grad(u), grad(v)) + inner(u, v)) * dx
L = inner(f, v) * dx

u = Function(V)

t = time.time()

# solve(a == L, u, solver_parameters={'ksp_type': 'cg', 'pc_type': 'none'})

# solve(a == L, u, solver_parameters={'ksp_type': 'cg', 'pc_type': 'ilu'})
solve(a == L, u, solver_parameters={'ksp_type': 'cg', 'pc_type': 'hypre', 'pc_hypre_type': 'boomeramg'})

elapsed = time.time() - t

print('Elapsed: {:.5e}'.format(elapsed))

File("helmholtz.pvd").write(u)

#try:
#  import matplotlib.pyplot as plt
#except:
#  warning("Matplotlib not imported")
#
#try:
#  fig, axes = plt.subplots()
#  colors = tripcolor(u, axes=axes)
#  fig.colorbar(colors)
#except Exception as e:
#  warning("Cannot plot figure. Error msg: '%s'" % e)
#
#try:
#  fig, axes = plt.subplots()
#  contours = tricontour(u, axes=axes)
#  fig.colorbar(contours)
#except Exception as e:
#  warning("Cannot plot figure. Error msg: '%s'" % e)
#
#try:
#  plt.show()
#except Exception as e:
#  warning("Cannot show figure. Error msg: '%s'" % e)

# exact solution
f.interpolate(cos(x*pi*2)*cos(y*pi*2))
print('Error L2 norm: {:.5e}'.format(sqrt(assemble(dot(u - f, u - f) * dx))))
